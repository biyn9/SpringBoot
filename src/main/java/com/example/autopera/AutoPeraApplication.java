package com.example.autopera;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutoPeraApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutoPeraApplication.class, args);
    }

}
